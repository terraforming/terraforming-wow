# Format Terraform files in Git pre-commit hook

Git pre-commit hook to call Terraform [format](https://www.terraform.io/docs/commands/fmt.html) on all ```*.tf``` files about to be committed.

## Setup

* Copy ```pre-commit``` script to ```.git/hooks``` directory and make it executable

        mkdir --parents .git/hooks
        cp --interactive pre-commit .git/hooks/
        chmod +x .git/hooks/pre-commit

## Notes

* Hook may be skipped with [--no-verify](https://git-scm.com/docs/git-commit#git-commit---no-verify) commit parameter

* It is hard to run Terraform [validate](https://www.terraform.io/docs/commands/validate.html) from Git hook, because it require to specify all parameters for validation to pass. With _-check-variables=false_ the validation only check if the file can be parsed correctly (what is already done in ```terraform fmt```)

## References

* Terraform [format](https://www.terraform.io/docs/commands/fmt.html) documentation
* Git [hooks](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks) documentation
* Git [pre-commit](https://github.com/git/git/blob/master/templates/hooks--pre-commit.sample) hook template
* Terraform format [pre-commit](https://gist.github.com/jamtur01/a567078b7ba545c3492f7cd32a65450d) hook gist
