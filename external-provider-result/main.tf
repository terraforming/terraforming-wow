resource "null_resource" "anything" {}

data "external" "script" {
  program = ["bash", "-c", "./script.sh"]

  # If required datasource may depend on a resource
  depends_on = ["null_resource.anything"]
}

output "string_from_script" {
  value = "${data.external.script.result["string"]}"
}

output "number_from_script" {
  value = "${data.external.script.result["number"]}"
}
