#!/bin/bash

# Simple flat JSON with only string values (no lists or maps)
cat << EOF
{
  "string": "Only string values are supported",
  "number": "1"
}
EOF