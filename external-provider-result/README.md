# Terraform external provider result

Demonstrate reading the result from the locally run script.

## Running

1. Create the deployment

        terraform init
        terraform apply -auto-approve

2. See the outputs

        Outputs:
        
        number_from_script = 1
        string_from_script = Only string values are supported

## Notes

Result from script must be a valid JSON. Only strings are supported (no arrays, no maps etc.). Use quotation marks around the numbers.

It is just an example that such things are possible. Usually **it is a better approach to pass the required values as parameters** and do not read them from external data source. But sometimes this example may get handy, though :)

## References

* Terraform [external provider data source](https://www.terraform.io/docs/providers/external/data_source.html) documentation
* Terraform [external provider proposal](https://github.com/hashicorp/terraform/issues/8144) documentation
