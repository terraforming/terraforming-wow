# Working with Terraform in Visual Studio Code

## Settings

Not only useful for Terraform files but if your target is a **\*nix** platform, enforce the compatibility with a sane people in your User Settings (_Ctrl+,_)

    "files.eol": "\n",


## Extension

The best is [Terraform extension](https://marketplace.visualstudio.com/items?itemName=mauve.terraform) by Mikael Olenfalk (GitHub [repository](https://github.com/mauve/vscode-terraform)). Any Snippets or Autocomplete extensions seems not to be so useful. This one is more than fine. Check the documentation for useful hotkeys (_Ctrl+T_, _Alt+F12_ etc.).

Switch on the automatic formatting on save by adding the following to your User Settings (_Ctrl+,_)

    "[terraform]": {
        "editor.formatOnSave": true,
    },

Introverts would like to also switch off the telemetry with

    "terraform.telemetry.enabled": false, 


## Tasks

Put the following to ```.vscode/tasks.json``` in your working directory. Then you can run Terraform directly from [Command Palette](https://code.visualstudio.com/docs/getstarted/userinterface#_command-palette) (_Tasks: Run Task_).

    {
        // See https://go.microsoft.com/fwlink/?LinkId=733558
        // for the documentation about the tasks.json format
        "version": "2.0.0",
        "type": "shell",
        "problemMatcher": [],
        "presentation": {
            "focus": false,
            "panel": "shared",
        },
        "tasks": [
            {
                "label": "terraform init",
                "command": "terraform init -upgrade",
            },
            {
                "label": "terraform plan",
                "command": "terraform plan",
            },
            {
                "label": "terraform apply",
                "command": "terraform apply",
                "presentation": {
                    "focus": true,
                }
            },
            {
                "label": "terraform output",
                "command": "terraform output",
            },
            {
                "label": "terraform destroy",
                "command": "terraform destroy",
                "presentation": {
                    "focus": true,
                }
            },
        ],
    }

You may also add the Keyboard shortcuts in ```keybindings.json```

    [
        {
            "key": "ctrl+t i",
            "command": "workbench.action.tasks.runTask",
            "args": "terraform init"
        },
        {
            "key": "ctrl+t p",
            "command": "workbench.action.tasks.runTask",
            "args": "terraform plan"
        },
        {
            "key": "ctrl+t a",
            "command": "workbench.action.tasks.runTask",
            "args": "terraform apply"
        },
        {
            "key": "ctrl+t o",
            "command": "workbench.action.tasks.runTask",
            "args": "terraform output"
        },
        {
            "key": "ctrl+t d",
            "command": "workbench.action.tasks.runTask",
            "args": "terraform destroy"
        },
    ]
