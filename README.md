# Terraforming WoW (Way of Warrior)

## [Best Practices](Best%20Practices.md)
Recommended rules to keep your work fun

## [Gitignore](.gitignore)
Ignoring the Terraform working files in Git repository

## [Git pre-commit hook](git-pre-commit-hook)
Git pre-commit hook to format Terraform files about to be committed

## [CloudFormation](cloudformation-stack)
How to create CloudFormation stack and read an outputs

## [External provider result](external-provider-result)
How to read a result from an arbitrary script

## [Module dependency](module-dependency)
How to define dependencies between modules

## [Spaces backend](spaces-backend)
Terraform S3 backend in DigitalOcean Spaces

## [Visual Studio Code and Terraform](vscode-terraform)
Recommended extensions and tasks configuration for [Visual Studio Code](https://code.visualstudio.com/)
