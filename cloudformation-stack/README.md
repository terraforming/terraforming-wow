# Terraform CloudFormation stack

Demonstrate how to create CloudFormation stack and read an outputs.

## Running

1. Create the deployment

        terraform init
        terraform apply

2. See the outputs

        Outputs:

        log_group_arns = [
                arn:aws:logs:us-east-1:xxxxxxxxxxxx:log-group:terraform-demo-file:*,
                arn:aws:logs:us-east-1:xxxxxxxxxxxx:log-group:terraform-demo-template:*,
                arn:aws:logs:us-east-1:xxxxxxxxxxxx:log-group:terraform-demo-inline:*
        ]
        log_group_names = [
                terraform-demo-file,
                terraform-demo-template,
                terraform-demo-inline
        ]
        stack_names = [
                terraform-log-group-stack-file,
                terraform-log-group-stack-template,
                terraform-log-group-stack-inline
        ]

## Notes

* Template can be in JSON or YAML format

* Template can be

  1. Read from file
  2. Rendered from [template_file](https://www.terraform.io/docs/providers/template/d/file.html)
  3. Inlined

* In CloudFormation [Intrinsic functions](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/intrinsic-function-reference.html) use two dollar signs to escape the Terraform interpolation (e.g. ```!Sub $${name}```)

* CloudFormation outputs are returned as a map

## References

* Terraform [CloudFormation](https://www.terraform.io/docs/providers/aws/r/cloudformation_stack.html) documentation
* CloudFormation [AWS::Logs::LogGroup](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-logs-loggroup.html) documentation
