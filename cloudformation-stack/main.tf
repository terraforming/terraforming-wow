provider "aws" {
  region = "us-east-1"
}

locals {
  log_group_name = "terraform-demo"
}

resource "aws_cloudformation_stack" "log_group_stack_file" {
  name = "terraform-log-group-stack-file"

  parameters {
    name = "${local.log_group_name}-file"
  }

  # Cannot use Terraform interpolation inside, just a CloudFormation parameters
  template_body = "${file("${path.module}/cloudwatch-log-group-file.yaml")}"
}

data "template_file" "cf_template" {
  template = "${file("${path.module}/cloudwatch-log-group-template.yaml")}"

  vars {
    log_group_name = "${local.log_group_name}-template"
  }
}

resource "aws_cloudformation_stack" "log_group_stack_template" {
  name = "terraform-log-group-stack-template"

  parameters {
    name = "${local.log_group_name}-template"
  }

  # Can use template_file (vars) interpolation inside, CloudFormation incristic functions must start with two dollar signs
  template_body = "${data.template_file.cf_template.rendered}"
}

resource "aws_cloudformation_stack" "log_group_stack_inline" {
  name = "terraform-log-group-stack-inline"

  parameters {
    name = "${local.log_group_name}-inline"
  }

  # Can use Terraform interpolation inside, CloudFormation incristic functions must start with two dollar signs
  template_body = <<TEMPLATE
---
AWSTemplateFormatVersion: "2010-09-09"
Description: Terraform can create CloudFormation stack

Parameters:
  name:
    Description: Log Group Name
    Type: String

Resources:
  exampleLogGroup:
    Type: AWS::Logs::LogGroup
    Properties: 
      LogGroupName: !Sub $${name}

Outputs:
  logGroupName:
    Description: Log Group Name
    Value: !Ref exampleLogGroup
  logGroupARN:
    Description: Log Group ARN
    Value: !GetAtt exampleLogGroup.Arn
TEMPLATE
}

output "stack_names" {
  value = [
    "${aws_cloudformation_stack.log_group_stack_file.name}",
    "${aws_cloudformation_stack.log_group_stack_template.name}",
    "${aws_cloudformation_stack.log_group_stack_inline.name}",
  ]
}

output "log_group_names" {
  value = [
    "${aws_cloudformation_stack.log_group_stack_file.outputs["logGroupName"]}",
    "${aws_cloudformation_stack.log_group_stack_template.outputs["logGroupName"]}",
    "${aws_cloudformation_stack.log_group_stack_inline.outputs["logGroupName"]}",
  ]
}

output "log_group_arns" {
  value = [
    "${aws_cloudformation_stack.log_group_stack_file.outputs["logGroupARN"]}",
    "${aws_cloudformation_stack.log_group_stack_template.outputs["logGroupARN"]}",
    "${aws_cloudformation_stack.log_group_stack_inline.outputs["logGroupARN"]}",
  ]
}
