# Terraform Best Practices

Basic rules to keep the Terraform files readable, maintainable and help to work with built resources.

## General

* All variables in [snake_case](https://en.wikipedia.org/wiki/Snake_case)
* Outputs in ```outputs.tf```
* Inputs in ```variables.tf```
* Resources in ```main.tf``` or same name as module or resource group (e.g.: ```vpc.tf``` or ```bastion.tf```)
* ```README.md``` in every directory with ```main.tf```
* Add description to inputs and outputs
* Before commit, format with [fmt](https://www.terraform.io/docs/commands/fmt.html) command or use Git [pre-commit](git-pre-commit-hook) hook
* Use [data sources](https://www.terraform.io/docs/configuration/data-sources.html) where possible (e.g. VPC may be obtained from [subnet](https://www.terraform.io/docs/providers/aws/d/subnet.html))
* Nested modules under the ```modules/``` subdirectory

## State

* Limit your blast radius by splitting your state into smaller parts
* Use [remote_state](https://www.terraform.io/docs/providers/terraform/d/remote_state.html) to read outputs from other deployments

## Backend

* To support team work store state in the [backend](https://www.terraform.io/docs/backends/index.html) (e.g. with [terraform-aws-s3-backend](https://bitbucket.org/terraforming/terraform-aws-s3-backend) module)
* Use one Backend per environment (development, test, stage, production etc.)

## Tagging

* Generate **unique** _Deployment ID_ (e.g. with [terraform-deployment-id](https://bitbucket.org/terraforming/terraform-deployment-id) module)
* Tag everything with generated _Deployment ID_
* This tagging mechanism has two reasons
    1. Tagging _mark_ all resources created by Terraform, if a state will break it will be easy to find them all and delete manually
    2. Tagging _bind_ the state with created resources, if a state will break it will be easy to find the resources **only from broken state** and delete them manually

## Modules

* Repository name with modules **must** starts with ```terraform-```
* ```main.tf``` in module root directory
* [Semantic versioning](https://semver.org/) for module release tags
* If sourcing modules not only from local path, do not forget to call Terraform [init](https://www.terraform.io/docs/commands/init.html) with ```-upgrade``` parameter

        terraform init -upgrade


## Git on Windows

If your target is a **\*nix** platform, enforce the compatibility with a [sane people](https://git-scm.com/book/en/v2/Customizing-Git-Git-Configuration#_code_core_autocrlf_code) with

    git config --global core.autocrlf input

It may be even disabled (set to _false_) but **do not set it to _true_**. Also, do not forget to set your editor accordingly.

## References

* Terraform [standard module](https://www.terraform.io/docs/modules/create.html#standard-module-structure) structure documentation
* Terraform [publishing modules](https://www.terraform.io/docs/registry/modules/publish.html#requirements) requirements documentation
* Disaster recovery [blog post](https://charity.wtf/2016/03/30/terraform-vpc-and-why-you-want-a-tfstate-file-per-env/)
