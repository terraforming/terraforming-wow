module "third" {
  source = "modules/third"

  depends_on = ["${module.second.done}"]
}

module "second" {
  source = "modules/second"

  depends_on = ["${module.first.done}"]
}

module "first" {
  source = "modules/first"
}
