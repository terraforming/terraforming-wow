# Dependencies between Terraform modules

Terraform (v0.11.10) doesn't support ```depends_on``` argument between child modules. Check the [#10462](https://github.com/hashicorp/terraform/issues/10462) for the better explanation why it is not supported and what was an original purpose of ```depends_on``` in resources specification.

## Defining dependencies between modules

With a suggested workaround from a discussion in [#10462](https://github.com/hashicorp/terraform/issues/10462#issuecomment-285733504), it is possible to specify the order how the modules (or better resources **within** modules) will be created.

Working with modules, Terraform just source all sub-modules from the root module and compute the **resources dependency graph** (try the [graph](https://www.terraform.io/docs/commands/graph.html) command). Because resources from sibling modules are not accessible, the trick is to put _most important_ resources in the outputs and use them as input do depending module. _Most important_ in this case mean modules which goes last (e.g. ```aws_iam_role_policy_attachment``` goes after ```aws_iam_policy``` and ```aws_iam_role```).

## Example

Module ```second``` depends on module ```first```. Without dependency specification the **order of resource creation is undefined**. Test to comment the ```depends_on``` variable in ```main.tf```, run ```terraform apply``` several times and check the content of ```output.txt```.

As it is not suitable to export everything from modules and use it in ```depends_on``` it is a good practice to define [null_resource](https://www.terraform.io/docs/provisioners/null_resource.html) depending on _most important_ resources from the module and export it in outputs. In general - the output must be computed **after** all resources on which you need to create a dependency exists.

In the example the ```module.first.null_data_source.done``` and ```module.second.null_resource.depends_on``` are resources preserving the dependency relationship.

All _most important_ resources from module ```second``` must _depends\_on_ ```module.second.null_resource.depends_on```. _Most important_ in this case means the resources which goes first (e.g. ```aws_launch_configuration``` goes before ```aws_autoscaling_group```).

Please note the ```triggers``` in ```module.second.null_resource.depends_on```. This resource must be created first thus it have to _depends\_on_ the resources from module ```first``` but because ```depends_on``` value cannot contain interpolations the ```triggers``` are used. **A resource cannot be created until all the inputs (i.e. ```triggers``` in this case) are not known.**

Update: added module ```third``` (depending on ```second```) for more reliable test results :)
