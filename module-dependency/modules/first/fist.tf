resource "null_resource" "file" {
  provisioner "local-exec" {
    command = "echo first >> ${path.root}/output.txt"
  }
}

data "null_data_source" "done" {
  inputs = {
    done = true
  }

  depends_on = ["null_resource.file"]
}

output "done" {
  value = "${data.null_data_source.done.outputs["done"]}"
}
