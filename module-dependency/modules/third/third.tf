variable depends_on {
  default = []

  type = "list"
}

resource "null_resource" "depends_on" {
  triggers {
    dependencies = "${join(", ", var.depends_on)}"
  }
}

resource "null_resource" "file" {
  provisioner "local-exec" {
    command = "echo third >> ${path.root}/output.txt"
  }

  depends_on = ["null_resource.depends_on"]
}
