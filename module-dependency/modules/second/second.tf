variable depends_on {
  type    = "list"
  default = []
}

resource "null_resource" "depends_on" {
  triggers {
    dependencies = "${join(", ", var.depends_on)}"
  }
}

resource "null_resource" "file" {
  provisioner "local-exec" {
    command = "echo second >> ${path.root}/output.txt"
  }

  depends_on = ["null_resource.depends_on"]
}

data "null_data_source" "done" {
  inputs = {
    done = true
  }

  depends_on = ["null_resource.file"]
}

output "done" {
  value = "${data.null_data_source.done.outputs["done"]}"
}
